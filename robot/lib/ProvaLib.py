import os.path
import subprocess
import sys
import sut.triangle
from robot.api.deco import keyword

class ProvaLib(object):
    def __init__(self):
        self.result = None

    def hello (self): 
        print ("Hello world")

        
    @keyword("stampa")
    def stampa_contenuto (self, args): 
        print(args)
        

    def create_compare(self, a,b,c): 
        tri = sut.triangle.triangle(int(a),int(b),int(c))
        type = tri.describe()
        return type
        
    
    create_compare.robot_name = "creazione triangolo"

    def somma(self, a, b): 
        return str(int(a) + int(b))

    def apici(self, a,b): 
        edges = [int(x) for x in list(a)]
        tri = sut.triangle.triangle(edges[0], edges[1], edges[2])
        type = tri.describe()
        return type