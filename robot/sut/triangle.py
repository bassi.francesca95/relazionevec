class triangle:

    def __init__(self, l1, l2, l3):
        self.a = l1
        self.b = l2
        self.c = l3

        if (check_tri(self)): 
            raise Exception("Not negative or x+y<z")

    def describe(self): 
        if (self.a == self.b and self.b == self.c): return "Equilatero"
        elif (self.a != self.b and self.b != self.c and self.c != self.a): return "Scaleno"
        else: return "Isoscele"
        
def check_tri(triangle): 
        if(not(triangle.a + triangle.b >= triangle.c and triangle.b + triangle.c >= triangle.a and triangle.c + triangle.a >= triangle.b)): 
            return True
        return triangle.a < 0 or triangle.b < 0 or triangle.c < 0 
