*** Settings ***
Documentation   Esempio base
Library           OperatingSystem
Library           lib/ProvaLib.py

*** Test Cases ***
testing
    
    Should Be Equal     10  10
    Should Be Equal     ${Esempio}  Esempio

prova hello
    hello

prova stampa
    stampac      ${Esempio}

prova stampa2
    ${result} =    hello    
    Should Be Equal     ${result}   Hello world

Test1
    creazione triangolo     2   2   2 

*** Variables ***
${Esempio}      Esempio

*** Keywords ***
stampac     
    [Arguments]     ${a}
    stampa          ${a}

Test triangolo 
    [Arguments]     ${a}    ${b}    ${c}    ${type}
    #${result} =     
    creazione triangolo     ${a}    ${b}    ${c} 

