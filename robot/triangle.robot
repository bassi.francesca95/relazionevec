*** Settings *** 
Documentation    Esempio su come usare Robot sull'esempio dei triangoli
Force Tags      test
Default Tags    triangoli
Library         lib/ProvaLib.py

*** Variables *** 
${SCALENO}  Scaleno
${LATI}     4 4 4 

*** Test Cases ***
Test1
    creazione triangolo     2   2   2 

Test Equilatero 
    [Tags]      creazione
    Test triangolo  2   2   2   Equilatero

Test Isoscele 
    Test triangolo   2  2   1   Isoscele
    Remove Tags      test

Test Scaleno
    Test triangolo  2   3   1   ${SCALENO}

Test Template con più valori
    [Template]  il triangolo con ${a} ${b} ${c} e un ${type}
    2   2   2   Equilatero
    2   2   1   Isoscele
    1   2   3   Scaleno 

Scrittura con gherkin
    Given stampa    Un triangolo
    When Test triangolo    2    2   2   Equilatero

Test triangolo con input tra apici
    Test triangolo apici    "2 2 2"     "Equilatero"

Start and end
    [Documentation]     caso di test con il for 
    ${result} =     Set Variable    ${EMPTY} 
    FOR     ${i}    IN RANGE    10
        ${result} =     somma   ${result}   ${i}
    END
    Should Be Equal    ${result}   45


*** Keywords *** 
Test triangolo apici "${value:[^"]+}" "${type}"
    apici   ${value}    ${type}

Test triangolo 
    [Arguments]     ${a}    ${b}    ${c}    ${type}
    ${result} =     creazione triangolo     ${a}    ${b}    ${c} 
    Should Be Equal     ${result}       ${type}

SetupKeyword 
    stampa Inizio 

il triangolo con ${a} ${b} ${c} e un ${type}
    Test triangolo      ${a}    ${b}    ${c}    ${type}

