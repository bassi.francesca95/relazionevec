public class Triangles {
        int a, b, c; 
    
        public Triangles (final int l1, final int l2, final int l3)
        {
            this.a = l1;
            this.b = l2; 
            this.a = l3; 
            if (check_tri())
                throw new IllegalArgumentException("Not negative or x+y<z");
        }
        
        private boolean check_tri ()
        {
            if (!(a + b >= c && b + c >= a && c + a >= b))
                return true;
            return a < 0 || b < 0 || c < 0;
        }
    
        public void describe()
        {
            if (a == b && b == c)
                System.out.println("Equilatero");
            else if (a != b && b != c && c != a)
                System.out.println("Scaleno");
            else
                System.out.println("Isoscele");
        }   
} 