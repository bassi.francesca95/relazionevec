# Specification Heading
tags: specification_test

This is an executable specification file. This file follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.

To execute this specification, run
	gauge run specs


* Vowels in English language are "aeiou".

## Vowel counts in single word

tags: single word
     vowel, vow

* The word "gauge" has "3" vowels.
* The word "aaaa" has "4" vowels.
* Have "4" vowels the word "AAAA"

## Vowel counts in multiple word

This is the second scenario in this specification

Here's a step that takes a table

* Almost all words have vowels
     |Word  |Vowel Count|
     |------|-----------|
     |Gauge |3          |
     |Mingle|2          |
     |Snap  |1          |
     |GoCD  |1          |
     |Rhythm|0          |
