# Zoo Spec Header
tags: animals, zoo

    |numero|
    |------|
    |1     |
    |-     |
    |2     |


* stampa il test "iniziato"

Come primo scenario creo un oggetto 
## Creare un animale con un nome
* generare un "Gorilla" 
* chiamarlo con il nome "Harambe"

Come secondo scenario conto le tuple della tabella fornita
## Esaminare la tabella di animali
Tags: tabella
* controllare che gli animali presenti siano "3"
    |animale| nome |
    |-----------|------|
    |Gorilla|Harambe| 
    |Pantera|Rosa|
    |Scoiattolo|Marshal|

Come terzo scenario leggo dei file 
## Sommare il numero di animali presenti nel CSV
Tags: files
* Contare gli animali presenti nel csv <table:/resource/zoo.csv>
* Ottenere il numero totale di animali nel txt <file:/resource/zoo.txt>

Come ultimo scenario testo un errore
## Test onfailure 
* Questo <numero> e davvero un numero?

___
* stampa il test appena "concluso"