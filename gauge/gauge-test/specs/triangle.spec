
# Test dei triangoli 
Suite di test per controllare che Triangle.java sia corretto

    |a  |b  |c  |type      |
    |---|---|---|----------|
    |4  |4  |4  |Equilatero|
    |2  |3  |4  |Scaleno   |
    |2  |2  |3  |Isoscele  |

## Equilatero
tags: tipo
* Generazione di un triangolo "Equilatero" con lati "2", "2", "2"

## Isoscele
tags: tipo
* Generazione di un triangolo "Isoscele" con lati "1", "2", "2"

## Scaleno
tags: tipo
* Generazione di un triangolo "Scaleno" con lati "1", "2", "3"

## triangolo con dubbi input
tags: eccezione
* Generazione di un triangolo non valido con lati "2.1", "2", "2"
* Generazione di un triangolo non valido con lati "-1", "1", "2"
* Generazione di un triangolo non valido con lati "6", "2", "2"

## Creazione di un triangolo
tags: tipo, tabella, csv

* Generare i triangoli del <table:/resource/triangle.csv>

* Generazione di un triangolo <type> con lati <a> <b> <c>

* Generazione di un triangolo e basta
    |a  |b  |c  |type      |
    |---|---|---|----------|
    |2  |2  |2  |Equilatero|
    |3  |2  |1  |Scaleno   |
    |2  |2  |1  |Isoscele  |