import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.TableRow;

import static org.assertj.core.api.Assertions.assertThat;

public class StepImplementation_triangle {
	@Step({ "Generazione di un triangolo <tipo> con lati <a> <b> <c>",
			"Generazione di un triangolo <Equilatero> con lati <2>, <2>, <2>"})
	public void generazioneDiUnTriangoloConLati(String type, String l1, String l2, String l3){
		int a = Integer.parseInt(l1); 
		int b = Integer.parseInt(l2);
		int c = Integer.parseInt(l3);
		Triangles triangle = new Triangles(a, b, c); 
		
		assertThat(triangle.describe()).isEqualTo(type);
    }

	@ContinueOnFailure(IllegalArgumentException.class)
	@Step("Generazione di un triangolo non valido con lati <6>, <2>, <2>")
	public void generazioneDiUnTriangoloNonValidoConLati(String l1, String l2, String l3){
		Triangles triangle = new Triangles(Integer.parseInt(l1), Integer.parseInt(l2), Integer.parseInt(l3)); 
		assertThat(triangle.describe()).isEqualTo("Isoscele");
    }

	@Step({"Generare i triangoli del <table:/resource/triangle.csv>","Generazione di un triangolo e basta <table>"})
	public void generareITriangoliDel(Table table){
		for (TableRow row : table.getTableRows()){
			String type = row.getCell("type");
			int a = Integer.parseInt(row.getCell("a"));
			int b = Integer.parseInt(row.getCell("b"));
			int c = Integer.parseInt(row.getCell("c"));
		

			Triangles triangle = new Triangles(a, b, c); 
			Gauge.writeMessage("type: "+ type + " triangle: "+triangle.getEdge());
			assertThat(triangle.describe()).isEqualTo(type);
		}
		
	}






	public class Triangles {
        int a, b, c; 
    
        public Triangles (int l1, int l2, int l3)
        {
            this.a = l1;
            this.b = l2; 
			this.c = l3; 

            if (check_tri())
                throw new IllegalArgumentException("Not negative or x+y<z");
        }
        
        private boolean check_tri ()
        {
            if (!(a + b >= c && b + c >= a && c + a >= b))
                return true;
            return a < 0 || b < 0 || c < 0;
		}
		
		public String getEdge(){
			return "a:"+a+" b:"+b+" c:"+c;
		}
    
        public String describe()
        {
            if (a == b && b == c)
                return ("Equilatero");
            else if (a != b && b != c && c != a)
                return ("Scaleno");
            else
                return ("Isoscele");
        } 


} 

}
