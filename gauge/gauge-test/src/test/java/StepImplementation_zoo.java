import com.thoughtworks.gauge.ContinueOnFailure;
import com.thoughtworks.gauge.Gauge;
import com.thoughtworks.gauge.Step;
import com.thoughtworks.gauge.Table;
import com.thoughtworks.gauge.datastore.*;

import static org.assertj.core.api.Assertions.assertThat;

public class StepImplementation_zoo {
    DataStore scenarioStore = DataStoreFactory.getScenarioDataStore();

    //test semplice con DataStore
	@Step("generare un <Gorilla>")
	public void generareUn(String animale){
        System.out.println("Generato un "+animale);
        scenarioStore.put("animale", animale);
	}

	@Step("chiamarlo con il nome <Harambe>")
	public void chiamarloConIlNome(String nome){
        System.out.println("Generato un "+scenarioStore.get("animale")+" con il nome "+nome);
        Gauge.writeMessage("Harambe vive su Gauge");
	}

    //Scenario con file
	@Step("Contare gli animali presenti nel csv <file:/resource/zoo.csv>")
	public void contareGliAnimaliPresentiNelCsv(Table csv){
        int num_bestie = csv.getColumnValues(1).stream().mapToInt(i -> Integer.parseInt(i)).sum();
        System.out.print("Il numero di bestie e " + num_bestie);
        Gauge.writeMessage("il numero di bestie e "+num_bestie);
	}

    @Step("Ottenere il numero totale di animali nel txt <file:/resource/zoo.txt>")
	public void ottenereIlNumeroTotaleDiAnimaliNelTxt(String num){
        System.out.print("Il numero di bestie e "+Integer.parseInt(num));
        Gauge.writeMessage("il numero di bestie e "+num);
    }
    
    //Scenario Tabella
	@Step("controllare che gli animali presenti siano <3> <table>")
	public void controllareCheGliAnimaliPresentiSiano3(int num_animali, Table table){
		assertThat(num_animali).isEqualTo(table.getTableRows().size());
	}

    // Aliasing Step e Context - TearDown
	@Step({"stampa il test <status>", "stampa il test appena <status>"})
	public void stampaIlTestIniziato(String status){
		System.out.println("TEST!----"+status);
	}

    @ContinueOnFailure(NumberFormatException.class)
	@Step("Questo <numero> e davvero un numero?")
	public void questoEDavveroUnNumero(String numero){
		Integer.parseInt(numero);
	}
}
