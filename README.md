# RelazioneVeC

# Cucumber

## Installazione
Prerequisiti: Intellij IDEA o Eclipse, JDK 8+, Gradle
Aggiungere al `build.gradle` la seguente configurazione e task
```
configurations {
    cucumberRuntime {
        extendsFrom testImplementation
    }
}

task cucumber() {
    dependsOn assemble, compileTestJava
    doLast {
        javaexec {
            main = "io.cucumber.core.cli.Main"
            classpath = configurations.cucumberRuntime + sourceSets.main.output + sourceSets.test.output
            args = ['--plugin', 'pretty', '--glue', 'hellocucumber', 'src/test/resources']
        }
    }
}
```
per verificare l'installazione eseguire: `gradle cucumber`

## Setup Progetto
La [documentazione](https://cucumber.io/docs/guides/10-minute-tutorial/) suggerisce di prendere il progetto di maven e renderlo di gradle. Per fare prima seguo quello che ha mostrato il professore alterando la struttura manualmente. 

#### Struttura progetto 
 Creare la cartella `cucumberTest`
* dentro ci creo il package degli steps `Test_Steps`
* importante creare la cartella resource con almeno `Test_Steps` nel nome
        in cui viene inserito il file `testing10.feature`
*  dentro le risorse deve avere un nome uguale o sottopackage delle Feature 

## Scrittura Specification
```
Feature: Descrizione ad alto livello della feature d
    Commento alla feature senza caratteri speciali o keywords
    
    Rule: 
    
    Scenario/Example: testo che identifica il gruppo di step
    */Given/Where/Then/And/But: definizione step
    
```
## Caratteristiche 
#### Feature
Si tratta di una descrizione ad alto livello della feature da testare, raggruppa degli Scenari. 
`Feature : descrizione della feature`

#### Scenario/Example
Si tratta di una keyword che permette di raggruppare steps, non c'è un limite al numero degli step, ma è consigliato usare 3-5 step per Scenario/Example. 
`Scenario : descrizione dello scenario`

#### Scenario Outline/Scenario Template
Permette di usare le DataTables usando nella descrizione parametri dinamici presi da una tabela definita: 
```
Scenario Outline: eating
  Given there are <start> cucumbers
  When I eat <eat> cucumbers
  Then I should have <left> cucumbers

  Examples:
    | start | eat | left |
    |    12 |   5 |    7 |
    |    20 |   5 |   15 |
```

#### Rule
Una Rule può contenere più scenari, è da implementare come gli step e provvede ad aggiungere informazioni per una feature. 

#### Steps
Cucumber esegue ogni step uno alla volta, in sequenza. Quando viene eseguito lo step viene cercata l'implementazione dello step sul file di implementazione degli step (file .java)

Gli step si definiscono con delle keyword `Given/Where/Then/And/But`, ma sono totalmente senza significato per cucumber, in quanto si possono ridurre al carattere `*` per definire uno step.  

#### Background
Altro sistema per raggruppare degli scenari. Serve per dare un contesto agli scenari e viene eseguito PRIMA DI OGNI SCENARIO, ma DOPO OGNI @Before HOOKS. 
Si consiglia di non usare Background per stati molto complessi. 

#### DocsStrings
Si tratta di un argomento che si può passare allo step, viene definito con il triplo doppio apice 
```
""" 
contenuto docstring 
"""
```
#### DataTables
Sistema per passare una tabella di variabili allo step, per ogni tupla della tabella viene rieseguito lo scenario. 

#### Tags

## Implementazione Step
L'implementazione degli step può essere organizzata in maniera flessibile. Infatti per Cucumber non importa dove si trovano ed è possibile creare un unico grande file con tutte le implementazioni dentro. 
Il template per le implementazioni delle feature trovate da cucumber pososno essere trovate come messaggio di errore all'esecuzione del task di cucumber. 
```
@Dato("che una mela costa {int} euro")
public void che_una_mela_costa_euro(Integer int1) {
    // Write code here that turns the phrase above into concrete actions
    throw new io.cucumber.java.PendingException();
}
```

## Esecuzione
Mediante il task definito su gradle.build o più semplicemente con 
`gradle cucumber`

Mediante l'uso di JUnit, TestNG e CLI si può forzare l'esecuzione parallela degli scenari. Tutti gli scenari di un file .feature verranno eseguiti su un thread a parte. 
Esempio con l'uso di [JUnit](https://cucumber.io/docs/guides/parallel-execution/#junit)
* Implementare lo step con un Thread
```
import io.cucumber.java.BeforeStep;
import io.cucumber.java.en.Given;

public class StepDefs {
    @Given("Step from {string} in {string} feature file")
    public void step(String scenario, String file) {
        System.out.format("Thread ID - %2d - %s from %s feature file.\n",
        Thread.currentThread().getId(), scenario,file);
    }
}
```
* Implementare il runner specificando con @RunWith 
```
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
public class RunCucumberTest {
}
``` 
* modificare nelle impostazioni della build installando il plugin Surefire 

## Report
Il report è su CLI, generato automaticamente dopo l'esecuzione del tast di cucumber, in cui vengono elencati gli step che sono passati ed evidenziati in verde. 
```
Scenario: Calcolo totale di acquisto di tre mele # Test_feature/testing10.feature:11
  Dato una ciliegia                              # Test_steps.Pam_steps.una_ciliegia()
  E costa 10 euro                                # Test_steps.Pam_steps.costa_euro(java.lang.Integer)
  Dato che una mela costa 4 euro                 # Test_steps.Pam_steps.che_una_mela_costa_euro(java.lang.Integer)
  Quando compro 2 mele                           # Test_steps.Pam_steps.compro_mela(java.lang.Integer)
  E compro 1 mela                                # Test_steps.Pam_steps.compro_mela(java.lang.Integer)
  Allora devo pagare 12 euro                     # Test_steps.Pam_steps.devo_pagare_euro(java.lang.Integer)
```
Il alternativa si possono usare dei plugin esterni per avere una interfaccia grafica mediante i .json generati da cucumber. (Esempio professore)

## Reference
[Documentazione](https://cucumber.io/docs/guides/)

--------------------------------------------------
# Gauge 
###### nota: l'IDE usato è VS CODE in quanto: 
> Deprecation Notice
VS Code has good support for Java and the Gauge VS Code works great with the Gauge java plugin. We are deprecating the IntelliJ Gauge plugin in favour of the Gauge VS Code plugin.  
The Gauge team will stop addding new features to the IntelliJ Gauge plugin, only bug fixes will be done henceforth. The Gauge team will officially end support for IntelliJ Gauge plugin in October 2020.  [fonte](https://github.com/getgauge/Intellij-Plugin)

## Installazione
Prerequisiti: 
JDK 8+ oppure .NET framework oppure Ruby 2.0+ (in base al linguaggio necessario)

Ci sono due alternative indipendenti dal sistema operativo per l'installazione di GAUGE
1. Installazione dal [sito ufficiale](https://docs.gauge.org/getting_started/installing-gauge.html) l'eseguibile per GAUGE e installazione del [plugin su VS CODE](https://marketplace.visualstudio.com/items?itemName=getgauge.gauge)

2. altrimenti 
installazione da [terminale](https://docsgaugeorg.readthedocs.io/en/master/installing.html)
`
    sudo apt install gauge
 `
installazione del plugin del linguaggio deisderato
`
    gauge install java
`
installazione del plugin per il report HTML
`
  gauge install html-report
`

## Setup Progetto
si inizializza il progetto da terminale con il comando
`gauge init java` 
o dal plugin di VSCODE dal terminale (ctrl+shift+p)
` Gauge: Create new Gauge Project`. 
Viene creato un progetto inizializzato in base al linguaggio specificato. La struttura del progetto è generata __by convention__. 

#### Struttura progetto 
`<project_root>`: 
Gauge contiene diverse cartelle nella root del progetto:

* __env__: contiene gli "environment" per le speciche cartelle, ogni cartella ha un file .property che definisce le variabili d'ambiente da usare durante l'esecuzione di una specification. 
* __logs__: contiene i file .logs che memorizzano i risultati dei vari `run spec`, cioè esecuzione delle specification
* __specs__: contiene i file `*.specs`, i file che contengono le specification, il linguaggio è markdown. Descrivono gli scenari
* __manifest.json__: è un file che contiene le informazioni del progetto, come i plugin, il linguaggio usato ecc. 

## Scrittura Specification
```
# Spec Heading
oppure
Spec Heading
============

    Tags: harambe
    
Si può scrivere qualsiasi commento nel mezzo se non definito da una kwyword o un simbolo riservato, non è necessaria l'indentazione.

* contex step

## Scenario 1
* primo step scenario 
* secondo step scenario 
* ...

Scenario 2
----------------
* primo step scenario 
* ...

___
* TearDown steps
```
## Caratteristiche 
nota: i nomi di Heading, Scenario, Steps (soprattuto nel step_text) non possono avere nel nome `<`, `>` e `" `  in quanto caratteri riservati.

### File Specification  
* __Spec Heading__: 
Deve contenere almeno uno scenario e può essere segnata con Tags. Si definisce con `#`.
* __Scenario__: 
Ogni scenario rappresenta un singolo flow di una particolare specification. Si definisce con `##`.
* __Steps__: 
Uno step è un elemento eseguibile della specification. Si definisce con `*`.

### Parametri 
Gli step possono avere dei parametri da passare all'implementazione stessa dello step. 
* `" "` definisce un valore statico come ad esempio può essere:  
`* creazione "gorilla" `  

* `< >` definisce un valore dinamico, come ad esempio può essere (mediante l'uso di una DataTable): 
`* creazione <animale> con dato <nome>`  
     |animale   | nome  |
     |------------|----------|
     | Gorilla | Harambe|
     | Pantera | Rosa|
    La dataTable può essere definita fuori dallo step o dentro lo step (non sarà visibile ad altri step)
* `<prefix:value>` definisce un parametro speciale. Il prefisso può essere una tabella o un file, il valore può essere il nome del file o il valore stesso del parametro. 
file: 
`* verifica che <file:animali.txt> sia visibile come <file:/felini/leoni.txt>`
csv: 
`* file per popolare il progetto <table:cuccioli.csv>`

* Le data table possono essere versatili perchè componnibili: 
```
#Create projects
|id| name |
|--|------|
|1 | john |
|2 | mike |

##First scenario
* Create the following projects
     |project name| username |
     |------------|----------|
     | Gauge java | <name>   |
     | Gauge ruby | <name>   |
```

### Tags 
I Tags servono molto banalmente ad associare una etichetta ad una specification o scenario. Si possono scrivere più Tags e su più righe (non c'è nessuna gerarchia nelle righe)
### Concept
Possibilità di ri-usare e combinare dei "gruppi logici" di step in una singola unity. Provvede un livello più alto di astrazione combinando gli step. 
Il __concept__ viene definito in un file `.cpt` che può essere posizionato un po' ovunque, ma generalmente è nella cartella `/spec`.
I concept possono essere usati all'interno di uno `spec` come qualsiasi step. Un singolo file `.cpt` può contenre la definizione di più concept. 

### Context e TearDown
Gli step __context__ e quelli di __tear down__ sono degli step definiti a priori dagli scenari. Sono eseguiti sempre prima e dopo OGNI scenario. Quindi l'esecuzione è: 
1. Step __Contex__
2. Step 1
3. Step __TearDown__
4. Step __Contex__
5. Step 2
6. Step __TearDown__
* __context__: sono eseguiti prima dello scenario, si definiscono dopo il Spec Header e prima degli scenari
* __tear down__: sono eseguiti dopo lo scenario, si definiscono dopo tre trattini bassi `___`

## Implementazione Step
L'implementazione delle specification è nella cartella 
`src/test/java/...`. L'implementazione dello step si può fare anche mediante l'ide passando con il mouse sopra il nuovo step. 

### Step Alias
Posso condensare più step in una unica implementazione:
```
  @Step(["creazione <animale> con dato <nome>", "creazione <animale>")
  public void presentation(animal_name):
      print("Animale: "+animal_name)
```
### Data Store 
Dato che Gauge ad ogni esecuzione ripulisce l'ambiente, si possono perdere delle variabili di cui si è interessati mantenere l'esistenza tra uno scenario e l'altro. Tre tipo di data store: 
1. __ScenarioStore__: Maniene in vita il dato per tutta l'esecuzione dello scenario (con l'esecuzione delle datatable): 
```
DataStore scenarioStore = DataStoreFactory.getScenarioDataStore();
scenarioStore.put("element-id", "455678");
```
2. __SpecStore__: Maniene in vita il dato per tutta l'esecuzione dello Specification (tra tutti gli scenari)
```
DataStore spectStore = DataStoreFactory.getSpecoDataStore();
specStore.put("element-id", "455678");
```
3. __SuiteStore__: Maniene in vita il dato per tutta l'esecuzione della sutie di test, nb: se viene eseguito in multithread/parallelo, i dati non saranno accessibili dai vari stream
```
DataStore suiteStore = DataStoreFactory.getSuiteDataStore();
suiteStore.put("element-id", "455678");
```
### Continue on Failure
Gauge al primo fallimento smette di eseguire, per evitare che si fermi sugli step, si può esplicitare all'implementazione dello step di continuare nonostante il fallimento, si fa con l'annotazione `@ContinueOnFailure`. 
Se a questa annotazioen vengono forniti degli argomenti `@ContinueOnFailure({AssertionError.class, CustomError.class})`, se gli errori catchati sono nell'insieme dei parametri, l'esecuzione continuerà. Se invece l'errore non è previsto dalle classi di eccezioni verrà fermata l'esecuzione del test.

### Hooks
Si possono implementare degli hook che permettono l'esecuzione di un certo metodo prima o dopo certi livelli di esecuzione della suite di test. 
Le annotazioni hanno questa forma: `@<After/Before><Suite/Scenario/Step>`

## Esecuzione
Due alternative
* da terminale
`gauge run specs`
* da Command Palette di VSCODE
`Gauge: Run Scenario`
* da intefaccia grafica premendo su `Run Spec` sopra la dichiarazione dello scenario o della spec heading

in tutti i casi è possibile definire quale spec e quale scenario eseguire in maniera esplicita definendolo come argomento nel comando

Da terminale ci sono diverse opzioni: 
* Esecuzione in parallelo `gauge run --parallel specs`
* Esecuzione di un spec specifico, specificandone anche le righe dell'input `gauge run --table-rows "1-3" specs/hello.spec`
* Esecuzione con Tags ` gauge run --tags tag1,tag2 specs`, si possono usare anche delle espressioni come `(TagA | TagB) & !TagC`

## Report
Gauge fornisce un interessante report grafico builtin con la sola necessità di installare il plugin per il report HTML. 
Si trova di default nella cartella: 
`<project_root>/reports/html-report/specs/example.html`

## Reference
* [Documentazione sito ufficiale](https://docs.gauge.org/index.html?os=windows&language=java&ide=vscode)
* [Documentazione-master](https://docsgaugeorg.readthedocs.io/en/master/index.html)
* [Repository Gauge](https://github.com/getgauge/gauge)

------------------------------------------------------------------
# Robot Framework

Framework opensource per l'automatizzazione dei test e per (RPA) robotic process automation. 
Si tratta di un framework attivamente supportato da diversi leader di mercato che lo usano nel proprio processo di sviluppo. Sviluppato inizialmente da Nokia, poi reso open source dal 2008.  

> Fate un favore a voi stessi, NON USATELO CON JAVA!

## Installazione
> python 
* Installare il framework con l'istruzione 
`pip install robot`
* assicurarsi di avere anche docutils
`pip install docutils`

## Setup Progetto
1. Creare un progetto Maven con maven-archetype-quickstart
2. Nel pom.xml
```
    <dependencies>
        <dependency>
            <groupId>org.robotframework</groupId>
            <artifactId>robotframework</artifactId>
            <version>3.2</version>
        </dependency>


        <dependency>
            <groupId>com.github.markusbernhardt</groupId>
            <artifactId>robotframework-selenium2library-java</artifactId>
            <version>1.4.0.8</version>
            <scope>test</scope>
        </dependency>
    </dependecies>
        
    <build>
      <plugins>
        <plugin> 
          <groupId>org.robotframework</groupId>
          <artifactId>robotframework-maven-plugin</artifactId>
          <version>1.4.6</version>
            <executions>
              <execution>
                  <goals>
                    <goal>run</goal>
                  </goals>
              </execution>
            </executions>  
        </plugin>
     </plugins> 
    </build>
```
[Guida installazione](https://medium.com/chaya-thilakumara/initial-step-to-start-writing-a-test-case-using-robot-framework-and-java-maven-4053e197bd7f) [Repository](https://github.com/chayathilak/Selenium-Robot)

[Guida Nokia](http://nokia.github.io/RED/help/user_guide/tools_integration/gradle.html)
#### Struttura progetto 
Non ha convenzioni

## Scrittura Specification
Il file `.robot` deve avere questa forma
```
*** Settings ***
Library <OperatingSystem/Screnshot/BuiltIn>
Library lib/RandomExternalLib.py

*** Test Cases ***
Commento ad un caso di test
    <Given/When/Then/And> commento dello step
    
Commento di un secondo caso di test
    [Tags] pasticci
    [Setup] Keyword di apertura
    Creating Object value   value2
    Creating Object value3  value4
    [Teardown] Keyword di chiusura

*** Keywords ***
 Creating Object value value 2
 
*** Variables ***
${USERNAME} pasticcio
${DATI_VARI} dr4tgf

```
## Caratteristiche 
#### *** Test Cases ***
I casi di test sfruttano keyword per definire gli step per testare la feature. I valori non sono posizionali e lo stile permette di scrivere liberamente testi come semplice testo anche da non addetti ai lavori.  

#### Annotazioni 
* [Documentation]: permette di specificare la documentazione del caso di test
* [Tags]: permette di applicare dei tag ai casi di test
* [Template]: specifica il template keyword da usare, il test conterrà solo i dati passati in argomento al template
* [Timeout]: Usato per applicare un timeout al caso di test

#### Data Driven test
Usando la parola `[Template]` viene cambiato il test case in un data driven test, in cui è possibile definire un set di input da eseguire in un caso di test
```
*** Test Cases ***
Invalid password
    [Template]    Creating user with invalid password should fail
    abCD5            ${PWD INVALID LENGTH}
    abCD567890123    ${PWD INVALID LENGTH}
    123DEFG          ${PWD INVALID CONTENT}
    abcd56789        ${PWD INVALID CONTENT}
    AbCdEfGh         ${PWD INVALID CONTENT}
    abCD56+          ${PWD INVALID CONTENT}
```
Si può usare in alternativa la keyword `Test Template`
#### *** Settings ***
Fornisce la keyword `Library` che permette di importare librerire che contengono implementazioni con linguaggi standard (Come Python e Java). Tra le librerie standard c'è `OperatingSystem`, `Screenshot` e `BuiltIn`. Altre librerie esterne come `Selenium2Library` per il web testing sono da installare separatamente. 

#### *** Keywords ***
Una feature molto potente che permette di creare delle keyword da altre keyword. La sintassi è chiamata `user-defined keywords` simile alla sintassi per creare i casi di test. 
```
#io sono un commento
*** Keywords ***
Clear login database
    Remove file    ${DATABASE FILE}

Create valid user
    [Arguments]    ${username}    ${password}
    Create user    ${username}    ${password}
    Status should be    SUCCESS
    
A user has a valid account
    Create valid user    ${USERNAME}    ${PASSWORD}
    
Login
    [Arguments]    ${username}    ${password}
    Attempt to login with credentials    ${username}    ${password}
    Status should be    Logged In
    
She can log in with the new password
    Login    ${USERNAME}    ${NEW PASSWORD}
```
#### *** Variables ***
Sono valori usati dentro i casi di test, definite a priori in una sezione a parte.
```
*** Variables ***
${USERNAME}               janedoe
${PASSWORD}               J4n3D0e
${NEW PASSWORD}           e0D3n4J
${DATABASE FILE}          ${TEMPDIR}${/}robotframework-quickstart-db.txt
${PWD INVALID LENGTH}     Password must be 7-12 characters long
${PWD INVALID CONTENT}    Password must be a combination of lowercase and uppercase letters and numbers
```
Esempio uso: 
```
*** Test Cases ***
User status is stored in database
    [Tags]    variables    database
    Create Valid User    ${USERNAME}    ${PASSWORD}
    Database Should Contain    ${USERNAME}    ${PASSWORD}    Inactive
    Login    ${USERNAME}    ${PASSWORD}
    Database Should Contain    ${USERNAME}    ${PASSWORD}    Active

*** Keywords ***
Database Should Contain
    [Arguments]    ${username}    ${password}    ${status}
    ${database} =     Get File    ${DATABASE FILE}
    Should Contain    ${database}    ${username}\t${password}\t${status}\n
```
#### Setup - Teardown
Per definire la sequenza di esecuzione di certe keyword, cioè per eseguirne alcune prima o dopo ogni caso di test, bisogna definire i setup e teardown nei settings. 
```
*** Settings ***
Suite Setup     Keyword con Setup ambiente test
Test Teardown   Keyword Pulizia ambiente test
```
Oppure mediante una annotazione `[Setup]` e `[Teardown]` 

#### Tags
Sempre nei settings è possibile definire anche i `Tags` per i casi di test. Ci sono due modi per associare i tags ai test:
* `Force Tags`: applicato a qualsiasi caso di test, si può rimuovere con `<Remove/Set> Tags` 
* `Default Tags`: se non ci sono Tags aggiunti con [Tags] vengono applicati di default quelli di default tags.
* `[Tags]`: per applicare al test dei tags specifici

#### Librerie e Variabili
Quando verrà eseguito il codice il risultato sarà: 
* KW1 = Robot
* KW2 = Robot eats Cucumber
* KW3 = two
```
class MyObject:

    def __init__(self, name):
        self.name = name

    def eat(self, what):
        return '%s eats %s' % (self.name, what)

    def __str__(self):
        return self.name

OBJECT = MyObject('Robot')
DICTIONARY = {1: 'one', 2: 'two', 3: 'three'}
*** Test Cases ***
Example
    KW 1    ${OBJECT.name}
    KW 2    ${OBJECT.eat('Cucumber')}
    KW 3    ${DICTIONARY[2]}
```


## Implementazione Step
L'implementazione richiede lo stesso nome della keyword, se la keyword 
Keyword: 
```
*** Settings ***
Library    MyLibrary

*** Test Cases ***
My Test
    Do Nothing
    Hello    world
    Login Via User Panel    ${username}    ${password}    

```
Implementazione:
```
def hello(name):
    print("Hello, %s!" % name)

def do_nothing():
    pass
    
@keyword('Login via user panel')
def login(username, password):
      # ...
      
def login(username, password):
  # ...

login.robot_name = 'Login via user panel'
```

Si possono associare i Tags nei metodi di implementazione con: `
@keyword(tags=['tag1', 'tag2'])`

## Esecuzione
Si esegue mediante riga di comando con `robot nomefile.robot` 

## Report
Robot genera automaticamente diversi report  
* CLI: sul terminale appare il risultato dei vari test
```
QuickStart                                                                           
==============================================================================        
User can create an account and log in                                 | PASS |       
------------------------------------------------------------------------------       
User cannot log in with bad password                                  | PASS |       
------------------------------------------------------------------------------        
User can change password                                              | PASS | 
------------------------------------------------------------------------------        
Invalid password                                                      | PASS |  
------------------------------------------------------------------------------        
User status is stored in database                                     | PASS |  
------------------------------------------------------------------------------        
QuickStart                                                            | PASS |        
5 critical tests, 5 passed, 0 failed                                                  
5 tests total, 5 passed, 0 failed                                              
==============================================================================        
```
* HTML: vengono generati due file HTML nella cartella di <root_project> 
     * `log.html`: Log grafico in cui si possono vedere per ogni scenario e per ogni step il risultato dell'esecuzione e il tempo trascorso. 
    * `report.html`: Report grafico in cui è possibile navigare filtrando per tags, suite di test e tipo (critico o test). I report hanno collegamenti con il file `log.html`

## Reference 
* Documentazione [QuickStart](https://github.com/robotframework/QuickStartGuide/blob/master/QuickStart.rst)
* Documentazione [UserGuide](http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html) (più completa)
